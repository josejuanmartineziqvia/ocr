import os
from processors.PDFProcessor import PDFProcessor

IN = 'todo'
OUT = 'done'


def process_todo():
    dirs = []
    for file in os.listdir(IN):
        in_dir = os.path.join(IN, file)
        out_dir = os.path.join(OUT, file)
        if os.path.isdir(in_dir):
            dirs.append(in_dir)
            if not os.path.exists(out_dir):
                os.mkdir(out_dir)

    for fdir in dirs:
        for filename in os.listdir(fdir):
            out_file = os.path.join(OUT, fdir, filename).replace(IN, '')
            if filename.endswith('.pdf'):
                with open(os.path.join(fdir, filename), 'rb') as f:
                    textfile = PDFProcessor.get_text(f)
                extension = '.pdf'
            elif filename.endswith('.jpg') or filename.endswith('.png'):
                # OCR
                continue
            elif filename.endswith('.txt'):
                with open(os.path.join(fdir, filename), 'r', encoding='utf-8') as f:
                    textfile = ''.join(f.readlines())
                extension = '.txt'
            else:
                # Not recognized
                continue

            with open(out_file.replace(extension, '.txt'), 'w', encoding='utf-8') as f:
                f.write(textfile)


if __name__ == '__main__':
    process_todo()
